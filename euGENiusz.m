close all
clear all
mtr_rodzic=zeros(5,24); %utworzenie 5 osobnikow 'rodzicow'
mtr_way=[1,2;1,3;1,4;1,5;2,3;2,4;2,5;3,4;3,5;4,5]; %sposby krzyzowania kazdy z kazdym
mut=[0.05 0.05 0.05 0.05 0.2 0.2 0.2 0.2 0.08 0.08 0.08 0.08 0.08 0.08 0.08 0.08 0.08 0.08 0.08 0.08 0.08 0.08 0.08 0.08]; %wagi mutacji dla kazdego bitu
for i=1:5
    for j=1:24
        mtr_rodzic(i,j)=randi(2)-1;
    end
end  % losowe osobniki poczatkowe
crosspattern=cp;    %wzorzec krzyzowania
mutacja=mutek;      %losowanie wag do mutacji
best=0; 
topgen=[];          %macierz najlepszych wynikow kazdej generacji
mtr_dzieci=krzyzuj(mtr_rodzic,crosspattern,mtr_way);    
for i=1:24
    if mut(1,i)>mutacja(1,i)
        for j=1:10
            mtr_dzieci(j,i)=toggle(mtr_dzieci(j,i));
        end
    end
end
% krzy�owanie ze sob� wszystkich osobnikow populacji pocz�tkowej
% zgodnie z wzorcem krzy�owania, krzyzowanie rownomierne
% dodatkowo mutacjia
params=zeros(10,3);
for i=1:10
    params(i,1)=(bin2dec(int2str(mtr_dzieci(i,1:8)))+1)/85;%masa
    params(i,2)=bin2dec(int2str(mtr_dzieci(i,9:16)))/2;%sprezystosc
    params(i,3)=bin2dec(int2str(mtr_dzieci(i,17:24)));%tlumienie
end     % dekodowanie parametrow eliminatora i przeskalowanie ich

survival=zeros(10,1);
for i=1:10
    survival(i,1)=darwin(params(i,1),params(i,2),params(i,3));
end     % ocena przystosowania na podstawie MSV

if max(survival)>best
    best=max(survival);
    [h,u]=max(survival);
    bsur=params;
    topgen(1,1)=best;
end
% zapis najlepszych osobnikow i ich pozycji
p=1;
while (best<7000 && p<50)
    
    best_guys=zeros(5,24);  %wybor 3 najlepszych z danej populacji
    ss=survival;
    for i=1:5
        [a,b]=max(ss);
        best_guys(i,:)=mtr_dzieci(b,:);
        ss(b,1)=0;
    end
    for i=1:2               % i dodatkowo 2 calkiem losowych zeby urozmaicic populacje
         for j=1:24
         best_guys(i+1,j)=randi(2)-1;
         end
    end
    crosspattern=cp;        % losowanie wzorca
    mutacja=mutek;          % losowanie wag mutacji
    
    mtr_dzieci=krzyzuj(best_guys,crosspattern,mtr_way);
    
    for i=1:24
        if mut(1,i)>mutacja(1,i)
            for j=1:10
                mtr_dzieci(j,i)=toggle(mtr_dzieci(j,i));
            end
        end
    end
    
    for i=1:10
        params(i,1)=(bin2dec(int2str(mtr_dzieci(i,1:8)))+1)/85;
        params(i,2)=bin2dec(int2str(mtr_dzieci(i,9:16)))/2;
        params(i,3)=bin2dec(int2str(mtr_dzieci(i,17:24)));
    end     % odczytanie parametrow eliminatora i przeskalowanie ich
    %figure(p+1);
    for o=1:10
        survival(o,1)=darwin(params(o,1),params(o,2),params(o,3));
    end     % ocena przystosowania na podstawie MSV
    
    if max(survival)>best
        best=max(survival);
        [h,u]=max(survival);
        bsur=params;
    end
    topgen(1,p+1)=max(survival);
    p=p+1;
    disp(['Best: ',num2str(best),' Pokolenie: ',num2str(p)]);
   
end
if best>6000
    disp(['Najlepszy wynik f.przystosowania : ',num2str(h)]);
    disp(['Masa m2: ',num2str(bsur(u,1)),' Sprezystosc k2: ',num2str(bsur(u,2)),' Tlumienie c2: ',num2str(bsur(u,3))]);
    figure(2)
    plot(topgen)
    title('Przebieg wyniku f.przystosowania')
    grid on
    rys(bsur(u,1),bsur(u,2),bsur(u,3));
    rys(3,440,5);
else
    gen
end
