function crosspatern=cp()
    crosspatern=zeros(1,24);
    for i=1:24
        crosspatern(1,i)=randi(2)-1;
    end
end