function MSV=darwin(m2,k2,c2)
    m1=10;c1=25;k1=1000;F=100;
    s=tf('s');
    M=(m1*s^2+c1*s+k1+c2*s+k2-((c2*c2*s^2+2*c2*k2*s+k2^2)/(m2*s^2+c2*s+k2)));
    x1=F/M;
    t = (0:0.01:10)';
    ox1=step(x1,t);
    st=ones(1001,1)*0.1;
    overshot=st-ox1;
    MSV=0;
    for i=1:1001
        MSV=overshot(i,1)^2+MSV;
    end
    MSV=MSV/1001;
    MSV=1/MSV;

    %plot(ox1)
    %hold on
    %grid on
end