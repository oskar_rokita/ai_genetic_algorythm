function mtr_dzieci=krzyzuj(mtr_rodzic,crosspattern,mtr_way)
mtr_dzieci=zeros(10,24);
    for i=1:10
        for j=1:24        
            if crosspattern(j)==1
                mtr_dzieci(i,j)=mtr_rodzic(mtr_way(i,1),j);
            else
                mtr_dzieci(i,j)=mtr_rodzic(mtr_way(i,2),j);
            end
        end
    end
end